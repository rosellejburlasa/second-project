<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
                [
                    'name'=> 'Abstract Strategy',
                    'created_at'=>now(),
                    'updated_at'=>now(),
                ],
                [
                    'name'=> 'Family',
                    'created_at'=>now(),
                    'updated_at'=>now(),
                ],
                [
                    'name'=> 'Card Games',
                    'created_at'=>now(),
                    'updated_at'=>now(),
                ]
            ]
        );
    }
}
