<?php

use Illuminate\Database\Seeder;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert(
    		[
		        [
		            'name'=> 'Sushi Go',
		            'serial'=> 'B01CETNKE2',
		            'price'=> '350',
		            'stock'=> 10,
		            'age'=> '8 years and up',
		            'featured'=> '/images/card/sushi1.JPG',
		            'includes'=> '118 cards and instruction',
		            'player'=> '2-8 Players',
		            'description'=> 
		            'Games for the Infinitely Imaginative Gamewright was founded in 1994 by four parents whose kids wanted great games. From the start, our mission has remained clear: Create the highest quality family games with outstanding play-value. Guided by themes and experiences that transcend age and salted with a bit of irreverence, our games are designed to foster laughter, learning, friendship and fun.

					Over the years our family has grown to over 150 games and countless happy players. The thousands of letters we receive from kids, parents, grandparents, and teachers tell us our hearts are in the right place. Thank you for inviting us into your home and for playing with Gamewright!',
		            'category_id'=> 3,
		            'created_at'=>now(),
                    'updated_at'=>now(),
		        ],
		        [			
		            'name'=> 'Codenames',			
		            'serial'=> 'TCIN:50364627',					
		            'price'=> '277',
		            'stock'=> 10,		
		            'age'=> '10 years and up',			
		            'featured'=> '/images/strategy/codename3.JPG',			
		            'includes'=> '16 agent cards in two colors, 1 double agent card, 7 innocent bystander cards, 1 assassin card, 40 key cards, 1 rulebook, 1 card stand, 1 time, and 200 cards with 400 codenames',			
		            'player'=> '2-8 Players',			
		            'description'=>			
		            'Codenames is a social word board game with a simple premise and challenging game play. Fast paced and a great party game thats sure to keep the whole family entertained, its inclusive game play makes the game challenging and fun!			
					
					The game starts as two rival spymasters know the secret identities of 25 agents. Their teammates know the agents only by their codenames. You can choose to compete in a team or by yourself, while you try to see who can make contact with all of their agents first!
					
					Pay attention as The Spymaster gives out a one word clue that can point to multiple words on the table. Teammates must try and decipher the clue to try and guess the words of their color while avoiding those that belong to the opposing team.
					
					Remember, pay close attention to clues and the spymaster but watch out and always avoid the assassin!
					
					The game works great with 4 players if you prefer to guess without help or you can add more players if you prefer a lively discussion. Players can also play a cooperative variant where a single team tries to achieve the highest score they can by playing against the game itself.',
		            'category_id'=> 1,			
		            'created_at'=>now(),			
                    'updated_at'=>now(),					
		        ],
		        [			
		            'name'=> 'The Game of Life',			
		            'serial'=> 'TCIN:53637252',				
		            'price'=> '480',
		            'stock'=> 10,			
		            'age'=> '12 years and up',			
		            'featured'=> '/images/strategy/Quarter1.JPG',			
		            'includes'=> 'gameboard, spinner and base, 100 cards, 4 cars, money pack, 4 savings tokens, 24 pegs, and instructions.',			
		            'player'=> '2-4 Players',			
		            'description'=>			
		            'In The Game of Life: Quarter Life Crisis board game you move around the board earning - and losing - money as you race to be the first to pay off your $500K soul-crushing debt. 			
					
					What milestones might you encounter in this hilarious adult parody edition of The Game of Life game? You will pay the consequences of some pretty outrageous situations such as finding a photo of Grandpas toe fungus, dropping your phone in the toilet, or calling in sick to binge-watch. 
					
					You might even get a new job, get a divorce, or get a botched tattoo! Who will be the first player to find for relief from their crippling debt to win the game? Get ready to laugh out loud when you break out The Game of Life: Quarter Life Crisis adult board game at parties and large and small get-togethers with friends.',
		            'category_id'=> 1,			
		            'created_at'=>now(),			
                    'updated_at'=>now(),					
		        ],			
		       	[			
		            'name'=> 'Exploding Kittens',			
		            'serial'=> 'B010TQY7A8',					
		            'price'=> '312',
		            'stock'=> 10,		
		            'age'=> '7 years and up',			
		            'featured'=> '/images/card/explodingkittens.JPG',			
		            'includes'=> '56 cards, Box and instructions.',			
		            'player'=> '2-5 Players',			
		            'description'=>			
		            'Exploding kittens is the perfect card game for adults, teens and kids who are into kittens and explosions and laser beams and sometimes goats.			
					More than 9 million copies sold, breaking records in kids games, adult games and everything in-between.
					A highly strategic, kitty-powered version of Russian roulette. Basically, if you draw an exploding kitten, you lose and you are full of loser sad-sauce. If you dont explode, you win! Congratulations, you are full of greatness!
					
					This “game of the year” award winner is the perfect holiday gift If you are looking for board games for adults but want to be able to play fun games with your kids as well.
					
					Its like UNO, except there are goats, magical enchiladas and kittens that can kill you. -CNN',
		            'category_id'=> 3,			
		            'created_at'=>now(),			
                    'updated_at'=>now(),					
		        ],
		        [			
		            'name'=> 'Monopoly',			
		            'serial'=> 'B00UB25IJA',					
		            'price'=> '285',
		            'stock'=> 10,		
		            'age'=> '17 years and up',			
		            'featured'=> '/images/strategy/GameOfThrone_Monopoly.jpg',			
		            'includes'=> '6 Oversized Collectible Tokens: Dragon Egg, Three-Eyed Raven, White Walker, Direwolf, Crown and The Iron Throne. Custom Designed Game Board features Famous Locations from the Television Series. Custom Villages and Keeps replace Traditional Houses and Hotels. Community Chest and Chance Cards renamed The Iron Throne and Valar Morghulis. Gold Dragons Themed Money. Instructions.',
		            'player'=> 'Maximum of 4 Players',			
		            'description'=>			
		            'An elegant, unique and special edition MONOPOLY game for Game of Thrones fans.

					All aspects of the game are customized; including oversized tokens, custom game board, cards, MONOPOLY money, rules and more!

					Perfect gift or activity for Game of Thrones fans.

					Play during the show or while you wait for the next season!',
		            'category_id'=> 1,			
		            'created_at'=>now(),			
                    'updated_at'=>now(),			
		        ],
		        [			
		            'name'=> 'Joking Hazard',			
		            'serial'=> 'B07K5BD5F4',						
		            'price'=> '450',
		            'stock'=> 10,	
		            'age'=> '17 years and up',			
		            'featured'=> '/images/card/Joking.JPG',			
		            'includes'=> '360 panel cards (including 10 add-your-own-words cards) combine to form millions of awful situations, and sometimes nice ones',			
		            'player'=> '2-10 Players',			
		            'description'=>			
		            'Color: Face Pattern			
					Joking Hazard is an extremely not-for-kids party game from the minds of Cyanide & happiness, The hit webcomic. Three or more players compete to build funny and terrible Comics about friendship, violence, sex, And everything in between. 360 panel cards (including 10 add-your-own-words cards) combine to form millions of awful situations, and sometimes nice ones. High quality cards can be burned for heat after society collapses.
					
					What makes this Joking Hazard special?
					A lot of "impress the judge" games create all the jokes for you by combining two random cards. 
					Joking Hazard is different, because there are THREE cards. That may not sound like a big difference, but it allows the judge to participate by inventing a story that everyone else has to finish. Context is everything. ',
		            'category_id'=> 3,			
		            'created_at'=>now(),			
                    'updated_at'=>now(),				
		        ],
		        [			
		            'name'=> 'Pandemic',			
		            'serial'=> 'TCIN:14203019',				
		            'price'=> '450',
		            'stock'=> 10,		
		            'age'=> '8 Years and Up',			
		            'featured'=> '/images/strategy/Pandemic.JPG',			
		            'includes'=> 'Game board, game pieces, markers, instructions',			
		            'player'=> '2-4 Players',			
		            'description'=>			
		            'You are the only team capable of saving humanity, think you have got what it takes as you play this fast paced & thrilling board game? Pandemic is a co-operative game where you and other players come together to stop the spreading of deadly viruses from across the globe!			
					
					Work together, cooperate & work fast as you share knowledge, treat diseases, & fly all over the world to prevent outbreaks and slow down the epidemic. The fate of humanity is in your hands!
					
					Start the game by placing the game board on a flat surface & placing the research station & four types of disease cubes on the board. Once the research station has been set up in Atlanta and all the disease cubes have been evenly distributed across the board you can start the game.
					
					Remember to use as many epidemic cards as you want the game to be challenging. Place the epidemic cards inside the player deck & proceed to place the player deck on the space provided on the board. Remember to always cooperate & work together & try and find a cure to save the world. The fate of all humans lies in your hands.',
		            'category_id'=> 1,			
		            'created_at'=>now(),			
                    'updated_at'=>now(),			
		        ],
		        [			
		            'name'=> 'Sequence',			
		            'serial'=> 'SN500B89',				
		            'price'=> '387',
		            'stock'=> 10,			
		            'age'=> '7 years and up.',			
		            'featured'=> '/images/family/sequence.JPG',			
		            'includes'=> '1 Folding Game Board, 2 Decks of Sequence Playing Cards, 135 Playing Chips, Complete Instructions',			
		            'player'=> '2-12 Players',			
		            'description'=>			
		            'Its fun, its challenging, its exciting, its Sequence!			
					Play a card from your hand, and place a chip on a corresponding space on the game board. When you have five in a row, its Sequence. Learn to block your opponents or remove their chips, and watch out for the Jacks - they are wild! With a little strategy and luck, you will be a winner.
					
					This exciting game of strategy is fun for the whole family.
					
					Great for individual or team play.
					
					Easy enough for kids and challenging enough for adults.',
		            'category_id'=> 2,			
		            'created_at'=>now(),			
                    'updated_at'=>now(),			
		        ]			
		    ]
	    );
    }
}
