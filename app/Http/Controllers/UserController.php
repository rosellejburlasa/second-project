<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Order;
use App\OrderItem;
use App\Transaction;
use App\Status;
use App\Category;
use DB;
use Session;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function asset(Request $request)
    {   
        $transaction=Transaction::all();
        return view('user.transaction')->with('transactions', $transaction);
    }

    public function receipt(Request $request)
    {
        $this->validate($request, [
            'borrowed_date' => 'required'
        ]);
        if (\Auth::check())
        {
            $cart = session()->get('cart');
            $item_ids = array_keys($cart);
            $items = Item::find($item_ids);
    
            $order = new Order;
            $order->user_id = auth()->user()->id;
            $order->price = $request->input('totalPrice');
            $order->save();
      
            foreach($items as $item)
            {
                $orderItems = new OrderItem;
                $orderItems->order_id = $order->id;
                $orderItems->item_id = $item->id;
                $orderItems->qty = $cart[$item->id];
                $orderItems->save();

                $transaction = new Transaction;
                $transaction->user_id = auth()->user()->id;
                $transaction->qty_rented = $cart[$item->id];
                $transaction->item_id = $item->id;
                $transaction->borrowed_date = $request->borrowed_date;
                $transaction->save();
    
                $request->session()->forget('cart.'.$item->id);
            }
    
            Session::flash('success', 'Thank you for shopping!');
            return redirect()->back();
        } 
        else 
        {
            return redirect()->route('login');
        }
        
    }

    public function index(){
        $items = Item::all();
        $categories = Category::all();
        return view('user.orders.index')->with('items', $items)->with('categories', $categories);
    }

    public function show($id){
        $items = Item::findOrFail($id);  
        return view('user.orders.show')->with('items', $items);
    }

    public function select(Request $request){
        $categories = Category::all();

        $category_filter = $request->category_filter;
        if($category_filter == "a-z") {
            $items = Item::all()->sortBy('name');
        } elseif($category_filter == "z-a") {
            $items = Item::all()->sortByDesc('name');
        } elseif($category_filter > 0){
            $items = Item::all()->where('category_id', $category_filter);
        } else {
            $items = Item::all();
        }

        return view('user.orders.index', compact('items', 'categories'));
    }

    public function search(Request $request)
    {   
        $categories = Category::all();
        $search = $request->get('search');
        $items = DB::table('items')->where('name','LIKE','%'.$search.'%')->paginate(5);
        return view('user.orders.index', ['items' => $items])->with('categories', $categories);
    }

    public function cancelled(Request $request, $id){
        $transaction = Transaction::findOrFail($id);

        $transaction->status_id=$request->input('status_id');
        $transaction->save();

        $records = DB::table('transactions')
            ->join('items', 'transactions.item_id', '=', 'items.id')
            ->select('transactions.item_id', 'items.name')
            ->get();

        return redirect()->back();
        // return view('user.transaction')->with('transaction', $transaction);
    }
}