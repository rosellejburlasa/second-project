<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Category;
use App\Item;
use App\User;
use Session;
use Auth;


class ProductController extends Controller
{
    public function __construct(){
        //The code below means that we are restricting access to
        //our views except for login and register
        $this->middleware('auth', ['except' => ['login', 'register']]);
    }
    public function index()
    {   
        $categories = Category::all();
        $items = Item::latest()->paginate(10);
        return view('admin.products.index',compact('items'))->with('categories', $categories);
    }


    public function create(){
        $categories = Category::all();
        return view('admin.products.create')->with('categories', $categories);
    }

    public function store(Request $request){
        $this->validate($request, [
            'name'=>'required',
            'serial'=>'required',
            'stock'=>'required',
            'price'=>'required',
            'stock'=>'required',
            'age'=>'required',
            'featured'=>'required|image',
            'includes'=>'required',
            'player'=>'required',
            'description'=>'required',
            'category_id'=>'required'
        ]);
        // dd($request->all());
        $item = new Item();
        $featured = $request->featured;
        $featured_new_name = time().$featured->getClientOriginalName();

        //MOve to the uploads folder
        $featured->move('uploads/products', $featured_new_name);
        $item->name=$request->name;
        $item->serial=$request->serial;
        $item->price=$request->price;
        $item->stock=$request->stock;
        $item->age=$request->age;
        $item->featured='/uploads/products/'. $featured_new_name;
        $item->includes=$request->includes;
        $item->player=$request->player;
        $item->description=$request->description;
        $item->category_id=$request->category_id;     
        $item->save();

        // $stock = new Stock();
        // $stock->current_stock=$request->current_stock;
        // $stock->item_id = $item->id;
        // $stock->save();

        Session::flash('success', 'You successfully created a Product');
        return redirect()->back();
    }

    public function show(Item $item){
        return view('admin.products.show', compact('item'));
    }




    public function edit($id){
        return view('admin.products.edit',compact('item'));
    }


    






    public function update(Request $request, Item $item){
        $request->validate([
            'name'=>'required',
            'serial'=>'required',
            'stock'=>'required',
            'price'=>'required',
            'age'=>'required',
            'featured'=>'required|image',
            'includes'=>'required',
            'player'=>'required',
            'description'=>'required',
            'category_id'=>'required'
        ]);
  
        $item->update($request->all());
        return redirect()->route('products.index')->with('success','Product updated successfully');
    }




    public function destroy(Item $item){
        // return 'This is Destroy Method';
        $item->delete();
        return redirect()->route('products.index')->with('success','Product deleted successfully');
    }

    public function select(Request $request)
    {
        $categories = Category::all();

        $category_filter = $request->category_filter;
        if($category_filter == "a-z") {
            $items = Item::orderBy('name', 'asc')->paginate(10);
        } elseif($category_filter == "z-a") {
            $items = Item::orderBy('name', 'desc')->paginate(10);
        } elseif($category_filter > 0){
            $items = Item::where('category_id', $category_filter)->paginate(10);
        } else {
            $items = Item::paginate(10);
        }

        return view('admin.products.index', compact('items', 'categories'));
    }
}
