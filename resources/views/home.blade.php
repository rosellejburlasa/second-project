@extends('layouts.custom')
  
@section('content')
<img class="container-fluid" src="{{ URL::to('/images/jumbotron.jpg') }}">
<div class="container my-5">
    <div class="row">
        <div class="col">
            @can('isAdmin')
                <div class="container text-center mt-4 pb-5">
                <h1 class="display-4">Hello {{ Auth::user()->name }}</h1>
                <div class="row mt-5 pt-2 text-center">
                    <div class="col-lg-3 col-6">
                        <div class="card text-white bg-secondary mb-3" style="max-width: 20rem;">
                          <div class="card-header"><h2 style="color:white;">Category</h2></div>
                          <div class="card-body">
                            <h4 class="card-title">Total:</h4>
                            <p class="card-text display-4">{{ \DB::table('categories')->count()}}</p>
                            <a href="{{route('categories.index')}}" class="btn btn-primary btn-lg">Details</a>
                          </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card text-white bg-info mb-3" style="max-width: 20rem;">
                          <div class="card-header"><h2>Users</h2></div>
                          <div class="card-body">
                            <h4 class="card-title">Total:</h4>
                            <p class="card-text display-4">{{ \DB::table('users')->count()}}</p>
                            <a href="#" class="btn btn-primary btn-lg">Details</a>
                          </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card text-white bg-dark mb-3" style="max-width: 20rem;">
                          <div class="card-header"><h2 style="color:white;">Products</h2></div>
                          <div class="card-body">
                            <h4 class="card-title">Total:</h4>
                            <p class="card-text display-4">{{ \DB::table('items')->count()}}</p>
                            <a href="{{route('products.index')}}" class="btn btn-primary btn-lg">Details</a>
                          </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card text-white bg-success mb-3" style="max-width: 20rem;">
                          <div class="card-header"><h2>Transaction</h2></div>
                          <div class="card-body">
                            <h4 class="card-title">Total:</h4>
                            <p class="card-text display-4">{{ \DB::table('transactions')->count()}}</p>
                            <a href="{{route('view')}}" class="btn btn-primary btn-lg">Details</a>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            @else
                <div class="row">
                    <div class="col-12 text-center h-25 py-5">
                        <h1 class="display-4">Welcome {{ Auth::user()->name }}</h1>
                    </div>
                </div>

                <!-- CAROUSEL -->
                <div class="top-content">
                    <div class="container-fluid">
                        <div id="carousel-example" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner row w-100 mx-auto" role="listbox">
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active">
                                    <img src="{{ URL::to('/images/homepage/1.JPG') }}" class="img-fluid mx-auto d-block" alt="img1">
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <img src="{{ URL::to('/images/homepage/2.JPG') }}" class="img-fluid mx-auto d-block" alt="img2">
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <img src="{{ URL::to('/images/homepage/3.JPG') }}" class="img-fluid mx-auto d-block" alt="img3">
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <img src="{{ URL::to('/images/homepage/4.JPG') }}" class="img-fluid mx-auto d-block" alt="img4">
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <img src="{{ URL::to('/images/homepage/5.JPG') }}" class="img-fluid mx-auto d-block" alt="img5">
                                </div>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                    <img src="{{ URL::to('/images/homepage/6.JPG') }}" class="img-fluid mx-auto d-block" alt="img6">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- Content -->
                <div class="row">
                    <div class="jumbotron mt-5 pt-5">
                          <h1 class="display-4 text-center" id="home"><strong>BOARD GAME RENTAL PROGRAM</strong></h1>
                          <p class="text-center">We are now renting board games! Need a game quickly for an event or party? Been dying to try that expensive game, but don’t want to drop all that money before you’ve played it? Try our rental program! Please take time to read through this page before attempting to rent games.
                        </p>
                        <hr class="my-4">
                        <div class="m-5 px-5 text-center"></div>
                            <h4>Here’s How It Works</h4><br>
                            <p>
                                We offer an extensive game library from which to rent. View the games available for rent below. Please note that this list may fluctuate, so a game that is available now may not be available at a later date.
                            </p>
                            <p>
                                When you decide to rent a game, you will fill out our board game game rental contract, which has the following stipulations:
                            </p>
                            <ol>
                                <li>
                                    You may not keep the game longer than 7 days. There is a minimum 2 day rental period. 
                                </li>
                                <li>
                                    You will leave your credit/debit card information on file with Bubut Games Rental. If you do not return the game within the specified rental period, we reserve the right to charge your credit/debit card for the full retail price of the game, and the game becomes yours.
                                </li>
                                <li>
                                    You agree to return the game in the EXACT condition in which you received it. If you attempt to return a game that is missing components or is damaged in any way, we reserve the right to charge your credit/debit card for the full retail price of the game.
                                </li>
                            </ol>
                      <a class="btn btn-primary btn-lg mt-5" href="{{route('orders.index')}}" role="button">Board Games Catalog</a>
                    </div>
                </div>
            @endcan
        </div>
    </div>
</div>
@endsection