@extends('layouts.custom')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="jumbotron bg-cover text-white" style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.6) 0%,rgba(0,0,0,0.6) 100%), url(https://images.pexels.com/photos/1634213/pexels-photo-1634213.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260)">
				<div class="container">
				  	<p class="lead text-center display-1">Get In Touch!</p>
				</div>
			</div> 
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 mb-5 text-center">
			<div class="card text-center mb-1">
			  <div class="card-body">
			  	<div class="row">
				  	<div class="col-lg-6">
				  		<img class="img-fluid" src="{{ URL::to('/images/location.svg') }}" style="height: 100px; width: 100px;">
				  	</div>
				  	<div class="col-lg-6">
					    <h5 class="card-text">
					    	<strong>
						    	Located in: Enzo
						    	<br>
								Address: 399 Sen. Gil J. Puyat Ave, Makati, 1200 Metro Manila
							</strong>
						</h5>
				  	</div>
				</div>
			  </div>
			</div>
			<div class="card text-center mb-1">
			  <div class="card-body">
			    <div class="row">
				  	<div class="col-lg-6">
				  		<img class="img-fluid" src="{{ URL::to('/images/phone.svg') }}" style="height: 100px; width: 100px;">
				  	</div>
				  	<div class="col-lg-6">
					    <h5 class="card-text text-center">
					    	<strong>Call us M-F 8:00 am to 6:00 pm EST<br/>(877)-555-5555</strong>
						</h5>
				  	</div>
				</div>
			  </div>
			</div>
			<div class="card  text-center">
			  <div class="card-body">
			    <div class="row">
				  	<div class="col-lg-6">
				  		<img class="img-fluid" src="{{ URL::to('/images/email.svg') }}" style="height: 100px; width: 100px;">
				  	</div>
				  	<div class="col-lg-6">
					    <h5 class="card-text">
					    	<strong>
					    		Email me at BUBUTGAMESRENTAL@gmail.com
					    	</strong>
						</h5>
				  	</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12 p-1 my-5 text-center">
			<div class="mapouter">
				<div class="gmap_canvas">
					<iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=zuitt%20makati&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
					</iframe>
				</div>
				<style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:800px;}
				</style>
			</div>
		</div>
	</div>
</div>
@endsection