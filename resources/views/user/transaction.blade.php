@extends('layouts.custom')

@section('content')
<div class="container my-5 py-5">
    <div class="row">
        <div class="col">
            <div class="jumbotron text-center">
              <h1 class="display-4">Thank you for shopping with us!</h1>
              <p class="lead">Your order has been placed and is being processed. You will received an email with the order details.</p>
              <hr class="my-4">
              <p>We hope you enjoy your purchase. Be sure to check out our other items!</p>
              <a class="btn btn-outline-danger" href="{{ route('orders.index') }}" role="button">Continue Shopping</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h2 class="text-center">Transaction History</h2>
            <h3 class="text-center" style="color:red;"><small>All Approved Orders will received an email from us!</small></h3>
          <table id="example" class="table table-striped table-hover dt-responsive nowrap" id="dataTable">
            <thead class="table-primary">
              <tr>
                <th>Products</th>
                <th>Quantity</th>
                <th>Borrowed Date</th>
                <th>Planned Returned Date</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($transactions as $transaction)
                <tr class="table-secondary">
                    <td><strong>{{$transaction->item->name}}</strong></td>
                    <td>{{$transaction->qty_rented}}</td>
                    <td>{{$transaction->borrowed_date}}</td>
                    <td>{{$transaction->returned_date}}</td>
                    <td>
                        @if($transaction->status_id == 1)
                            Pending
                        @elseif($transaction->status_id == 2)
                            Cancelled
                        @elseif($transaction->status_id == 3)
                            Approved
                        @elseif($transaction->status_id == 4)
                            Rejected
                        @else
                            Returned
                        @endif
                    </td>
                    @if($transaction->status_id == 1)
                    <td>
                        <form method="POST" action="{{ route('transaction.cancelled',['id'=>$transaction->id]) }}">
                            @csrf
                            <input type="hidden" name="status_id" value="2">
                            <button class="btn btn-primary" type="submit">Cancelled</button>
                        </form>
                    </td>
                    @else
                        <td> No action needed!</td>
                    @endif
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
</div>
@endsection
