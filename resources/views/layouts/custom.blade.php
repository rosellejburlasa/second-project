@php
    $cart_quantity = 0;
    $cart = session('cart');

    if (!empty($cart)){
        $cart_quantity = array_sum($cart);
    }else{
        $cart_quantity = 0;
    }
@endphp
<!doctype html>
<html lang="en">
<head>
    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Roselle J. Burlasa">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title -->
    <title>{{ config('app.name') }}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/icons/favicon.ico') }}">

    <!-- Fontawesome -->
    <script src="https://kit.fontawesome.com/54dd48c648.js" crossorigin="anonymous"></script>

    <!-- Bootswatch -->
    <link href="https://bootswatch.com/4/superhero/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- Animate CSS-->
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Toastr CSS -->
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">

</head>

</head>
<body>
<div class="wrapper d-flex align-items-stretch">
	<nav id="sidebar">
		<div class="p-4 pt-5">
            @can('isAdmin')
	  		<a href="#" class="img logo rounded-circle mb-5" style="background-image: url({{ URL::to('/images/photo.jpg') }});"></a>       
            <div class="text-center"><span class="user-name"><strong>{{ Auth::user()->name }}</strong></span></div>
            <div class="text-center"><span class="user-role">Administrator</span></div>
                <ul class="list-unstyled components mb-5">            
                    <li class="active">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-info-circle"></i> Products</a></a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li>
                                <a href="{{route('products.create')}}"><i class="fas fa-plus"></i> Add Products</a>
                            </li>
                            <li>
                                <a href="{{route('products.index')}}"><i class="far fa-eye"></i> View Products</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-info-circle"></i> Categories</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li>
                                <a href="{{route('categories.create')}}"><i class="fas fa-plus"></i> Add Categories</a>
                            </li>
                            <li>
                                <a href="{{route('categories.index')}}"><i class="far fa-eye"></i> View Categories</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{route('home')}}"><i class="fas fa-chess-rook"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="{{route('orders.index')}}"><i class="far fa-list-alt"></i> Catalog</a>
                    </li>
                    <li>
                        <a href="{{route('view')}}"><i class="fas fa-cart-plus"></i> Transaction</a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault(); 
                            document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i> Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>                
                </ul>
            @else
                <a href="#" class="img logo rounded-circle mb-5" style="background-image: url({{ URL::to('/images/photo.jpg') }});"></a>       
                <div class="text-center"><span class="user-name"><strong>{{ Auth::user()->name }}</strong></span></div>
                <div class="text-center"><span class="user-role">User</span></div>
                <ul class="list-unstyled components mb-5">            
                    <li class="active">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-home"></i> Home</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li>
                                <a href="{{route('home')}}">Board Game Rental Program</a>
                            </li>
                            <li>
                                <a href="{{route('transaction')}}">Transaction</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{route('orders.index') }}"><i class="far fa-list-alt"></i> Catalog</a>
                    </li>
                    <li>
                        <a href="{{ route('carts.all') }}"><span class="badge badge-pill badge-info">{{$cart_quantity}}</span> <i class="fas fa-cart-plus"></i> Cart</a>
                    </li>
                    <li>
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-info-circle"></i> About us</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li>
                                <a href="{{route('about')}}"><i class="fas fa-info-circle"></i> About us!</a>
                            </li>
                            <li>
                                <a href="{{route('contact')}}"><i class="fas fa-address-book"></i> Contact Us!</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault(); 
                            document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i> Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>                
                </ul>
            @endcan

            <div class="footer">
            	@include('partials.footer')
            </div>
        </div>
   </nav>

    <!-- Page Content  -->
    <div id="content">
        <p class="text-center pt-4">COVID-19: Please note that delivery of products may be delayed. Thank you for your patience!</p>
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
          <div class="container-fluid">
            <button type="button" id="sidebarCollapse" class="btn btn-primary">
              <i class="fa fa-bars"></i>
              <span class="sr-only">Toggle Menu</span>
            </button>
<!--             <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button> -->
            <img class="img-fluid pl-3 ml-auto" src="{{ URL::to('/images/logo.png') }}" style="height:50px;">
    		<a class="navbar-brand ml-auto" href="#"><strong>Bubut Games</strong></a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            </div>
          </div>
        </nav>
        <main>
        	@yield('content')
    	</main>
	</div>
</div>


<!-- Bootstrap -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>


<!-- Custom JS -->
<script src="{{ asset('js/script.js') }}"></script>

<!-- Toastr JS -->
<script src="{{ asset('js/toastr.min.js') }}"></script>

<script type="text/javascript">
    @if(Session::has('success'))
        toastr.success(" {{ Session::get('success') }} ")
    @endif
</script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>
</html>