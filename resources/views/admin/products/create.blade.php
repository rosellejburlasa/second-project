@extends('layouts.custom')

@section('content')
	<div class="container my-5 py-3">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				@if ($errors->any())
			    <div class="alert alert-danger">
			        <strong>Whoops!</strong> There were some problems with your input.<br><br>
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
				@endif
				<br>
				<div class="card mx-5">
					<div class="card-header" style="background-color: orange;">
						<h4><strong>Add Products</strong></h4>
					</div>
					<div class="card-body">
						{{-- Form for creating a post --}}
						<form method="post" action="{{route('products.store')}}" enctype="multipart/form-data">
							@csrf
							<div class="form-group">
								<label>Name</label>
								<input type="text" name="name" class="form-control">
							</div>
							<div class="form-group">
								<label>Serial</label>
								<input type="text" name="serial" class="form-control">
							</div>
							<div class="form-group">
								<label>Stock</label>
								<input type="number" name="stock" class="form-control">
							</div>
							<div class="form-group">
								<label>Price</label>
								<input type="number" name="price" class="form-control">
							</div>
							<div class="form-group">
								<label>Allowed Age</label>
								<input type="text" name="age" class="form-control">
							</div>
							<div class="form-group">
								<label>Images</label>
								<input type="file" name="featured" class="form-control p-1" >
							</div>
							<div class="form-group">
								<label>Includes</label>
								<textarea class="form-control" name="includes" cols="5" rows="6"></textarea>
							</div>
							<div class="form-group">
								<label>Number of players allowed</label>
								<input type="text" name="player" class="form-control">
							</div>
							<div class="form-group">
								<label>Description</label>
								<textarea class="form-control" name="description" cols="5" rows="6"></textarea>
							</div>
							<div class="form-group">
								<label>Category:</label>
								<select class="form-control" name="category_id">
									<option disabled selected>Select the Category</option>
									@foreach($categories as $category)
										<option value="{{$category->id}}">{{$category->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<button class="btn btn-primary" type="submit">Submit Products</button>
							</div>	
						</form>
						<a class="btn btn-outline-primary" href="{{ route('products.index') }}"> Back</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection